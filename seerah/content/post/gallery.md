---
title: "Gallery: Our Beautiful Family"
date: 2019-09-05T12:14:34+06:00
image: "images/post/post-3.jpg"
author: "Faraaz Yar Khan" # use capitalize
description: "This is the video gallery."
categories: ["video"]
tags: ["videos", "seerah"]
draft: false
---


I am thinking if we can put a family picture (one per family), where family is defined as people who
are living together in one house during the seerah sessions (so Naveed bhai and fam, kishwar aunty
and fam, naeem bhai and fam etc.), we can put their pictures here memoralizing all who attended.
