
---
title: "Winners: Quiz Winners from our Seerah Series"
date: 2019-09-12T12:14:34+06:00
image: "images/post/winners.jpg"
author: "Faraaz Yar Khan" # use capitalize
description: "This is the winners gallery."
categories: ["video"]
tags: ["videos", "seerah"]
draft: false
---
Alhamdulillah on 4th October, 2020 the family held a ceremony honoring all participants and
recognizing the following top ten winners:

| Participant             | Rank | Quiz 1 | Quiz 2 | Grand Quiz | Weighted Score |
|-------------------------|------|--------|--------|------------|----------------|
| Aalaa Rasheed Yar Khan  |    1 |    485 |    681 |        841 |            712 |
| Abdur Rahman (Firasath) |    2 |    444 |    288 |        871 |          618.5 |
| Nibras Yar Khan         |    3 |    308 |    838 |        588 |          580.5 |
| Iffath Yar Khan         |    4 |    371 |    418 |        693 |         543.75 |
| Rehma Zia               |    5 |      0 |    211 |        633 |         369.25 |
| Sumayya                 |    6 |    141 |    274 |        377 |         292.25 |
| Adiba Yar Khan          |    7 |    219 |    213 |        352 |            284 |
| Kamran Yar Khan         |    8 |    406 |    213 |        238 |         273.75 |
| Zainab Omer Yar Khan    |    9 |    150 |    149 |        244 |         196.75 |
| Amani Yar Khan          |   10 |    116 |    184 |        228 |           189  |


# Sahaba Series Quiz Results
<br/>
<br/>

## Quiz 1: Abu Bakr Siddiq RA

<br/>

**Participant**|**Rank**
:-----:|:-----:
Iffath|1
Adiba|2
Sumaiya|3
Kamran|4
Nibras|5
Rehma|6
Asna|7
Habeeba|8
Riyaz|9
Aamena|10


<br/>
<br/>

## Quiz 2: Umar RA
<br/>

**Participant**|**Rank**
:-----:|:-----:
Aalaa|1
Iffath|2
Adiba|3
Sumaiya|4
Rehma|5
Kamran|6
Fatima|7
Maymoona|8
Abdullah|9
Riyaz|10

<br/>
<br/>

# Quiz 3: Uthman RA
<br/>

**Participant**|**Rank**
:-----:|:-----:
Ahmed|1
Kamran|2
Adiba|3
Wasif|4
Sumaiya|5
Nibras|6
Rehma|7
Nizam|8
Abdullah|9
Zainab Omer|10
<br/>
<br/>
