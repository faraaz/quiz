---
title: "Announcing Quiz Teams"
date: 2021-01-05T12:14:34+06:00
image: "images/post/all-hands.jpg"
author: "Faraaz Yar Khan" # use capitalize
description: "Quiz Teams"
categories: ["quiz"]
tags: ["quiz", "seerah"]
draft: false
---

<br/>

## Background
<br/>

The core team is constantly thinking about how to make our sessions even more fun, interactive,
competitive and engaging, all in the way of Allah SWT, insha Allah.

Some of our best ideas though, come from unexpected places.

Iffath Yar Khan many weeks ago had suggested we add a quiz to the session, and boy! What a success
that has been!!

Then this week, the seemingly quiet Ahmed Rasheed Yar Khan proposed why not make this whole quiz
thing a team sport!!!

It immediately sounded amazing and all of us jumped at it with enthusiasm, and started talking through how to best do
that, this post will outline our proposed strategy for Quiz Teams!!

<br/>

## How it works
<br/>

First things first, we absolutely want to continue rewarding individual excellence, so the prizes
for the top three performers in every quiz, remain intact. If you win a spot in the top three, you
get rewarded regardless of how your team performs.


Now here is where it gets interesting, we have divided the regular quiz participants into two teams:

1. Team <span style="color:red">Shamsi :sun_with_face: </span> and
1. Team <span style="color:blue">Qamari :first_quarter_moon_with_face: </span>

We carefully chose teams in a way that would keep this competitive, we tried to distribute
historically strong players (based on past quiz performances) evenly across the two teams so the
game is evenly poised!

<br/>


### Prize Distribution
<br/>

Individual winners will get the same prizes as before which is $100 (or equivalent) for the first
spot, $75 for the second and $50 for the third.

Participating members of the winning team, who are NOT in the top three, will divide the pool of
$275, upto a maximum winning of $25 per person (no prize
for those who do not participate). Any amount left from the team pool carries forward to the next
  quiz.

Yup its that simple!

So you only have two objectives:

1. Score as many points as you can during the quiz to gain a spot in the top three
1. Motivate your team members to join every quiz and to prepare for it, so they score as many points as they can to help your team win (in case you end up outside the top three)

### The Teams
<br/>


| **Team Shamsi :sun_with_face:** |
| -----------                     |
| Aalaa                           |
| Aamena                          |
| Abdullah                        |
| Aneesa                          |
| Arshia Hussain                  |
| Mohammed (Habeeba )             |
| Maqdoom                         |
| Hasan                           |
| Imran                           |
| Jawad                           |
| Kamran                          |
| Kashif                          |
| Kubra                           |
| Naveed                          |
| Nawaz                           |
| Nazia                           |
| Nibras                          |
| Sameera Omer                    |
| Sumaiya                         |
| Tabassum                        |
| Wasif                           |

<br/>

| **Team Qamari** :first_quarter_moon_with_face: |
| -------------                                  |
| Iffath        |
| Abdur Rahman  |
| Adiba         |
| Adnaan        |
| Ahmed         |
| Amani         |
| Arshia Faisal |
| Asna Hussain  |
| Faisal        |
| Faizaan       |
| Farah         |
| Fatima        |
| Hafsa Omer    |
| Nazneen       |
| Nizam         |
| Rehma         |
| Riyaz         |
| Samia Shaikh  |
| Shabrez       |
| Zainab Omer   |

<br/>

May Allah SWT help all of us maintain ikhlaas in our niyyah and increase our knowledge and understanding of the deen. Ameen

Now, Let the games begin!

Jazak Allahu Khairan
