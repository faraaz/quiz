---
title: "Speaker Schedule for the Yar Khans Platform"
date: 2023-08-13T12:14:34+06:00
image: "images/post/speakers.jpg"
author: "Faraaz Yar Khan" # use capitalize
description: "Our schedule"
categories: ["schedule"]
tags: ["schedule", "seerah"]
draft: false
---

| Date                    | Speaker       | Surahs                                                                 |
|-------------------------|---------------|------------------------------------------------------------------------|
| Saturday, July 6, 2024  | Asna Hussain     | Surah Takwir (Ayat 1 - End) to Surah Tariq (Ayat 1 - End)              |
| Saturday, July 13, 2024 | Sumayya KYK   | Surah Ala (Ayat 1 - End) to Surah Balad (Ayat 1 - End)                 |
| Saturday, July 20, 2024 | Fatima Zia    | Surah Shams (Ayat 1 - End) to Surah Teen (Ayat 1 - End)                |
| Saturday, July 27, 2024 | Imran AYK     | Surah Alaq (Ayat 1 - End) to Surah Kafirun (Ayat 1 - End)              |
| Saturday, August 3, 2024| Jawaad HYK    | Surah Nasr (Ayat 1 - End) to Surah Naas (Ayat 1 - End)                 |
| Saturday, August 10, 2024| Iffath AYK    | Bayan ul Quran Ending Speech Part 2 / Khatam Al Quran                 |



### Live Sheet
You can view/edit the speaker schedule here: [Post Seerah Sessions](https://docs.google.com/spreadsheets/d/1YHME_zZBuLLJs_sHZuYUrE37inwnslYRogflKhQv6ZM/edit?usp=sharing)
<iframe width="100%" height="100%" src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQA7Uz8HtR82ZB5zF7ZjiOfnst_ONYPLZBXf_Ym54O5fI0oXUrfxtsuApF_0WdafLkCKnoODuUh-ydN/pubhtml?gid=1761475030&amp;single=true&amp;widget=true&amp;headers=false"></iframe>
