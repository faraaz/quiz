---
title: "Seerah Quiz: The Details"
date: 2019-09-05T12:14:34+06:00
image: "images/post/post-4.jpg"
author: "Faraaz Yar Khan" # use capitalize
description: "This is meta description."
categories: ["quiz"]
tags: ["quiz", "seerah"]
draft: false
---


<br/>

## Weekly Quiz & Sahabi Quiz

<br/>

Weekly quiz will be held on all Saturdays for seerah sessions. The topic for each quiz will be all Seerah
Topics covered up until the Quiz Time (including the topic being covered that very day).

For Khalifa Sessions, a quiz will be held on the last episode for a given Khalifa, for example, the
last episode of Abu Bakr Siddiq RA, will have a quiz on all topics covered on Abu Bakr Siddiq RA.

For Sahabi episodes we will conduct a quiz once every three weeks and each quiz will cover topics
covered since the previous quiz.

There will be a grand quiz for the Ashratu Mubashira on 20th March, 2021. The topics for the grand
quiz will be all Sahaba in the Ashratu Mubashira


<br/>
<br/>

### Upcoming Quiz Dates

<br/>

| Date | Quiz Topics |
|--------|------------|
| 12-Jun-2021 | Abdullah Ibn Masud RA, Jabir Bin Abdullah RA, Zayd Ibn Harith RA and family |
| 3-Jul-2021 | Anas Ibn Malik RA, Muadh Ibn Jabal RA, Ubay Ibn Ka'ab RA, Amr Ibn Al Aas RA and
Abdullah Ibn Amr Ibn al Aas RA |

Each quiz will run for *6 minutes* and will have between 50 and 70 questions. Answering all
questions is not necessary, the results are based on the number of points earned.

There is a penalty for wrong answers, so be careful when guessing.
<br/>
<br/>

### Prizes

<br/>

Going forward Prizes will be awarded based on the new Quiz Teams Model, see the
[Teams](https://seerah.yarkhans.com/post/quiz-teams.md) for more details

<br/>
<br/>

#### Individual Prizes

| Rank | Prize in USD | Prize in INR | Prize in SAR |
| ------|--------------|--------------|--------------|
| 1st  | $100         | INR 7,500   | SAR 375 |
| 2nd  | $75 | INR 5,500   | SAR 280      |
| 3rd  | $50| INR 4,000   | SAR 190      |

<br/>

#### Team Prizes

All members of the winning team (except those who have already received an individual prize by
achieving a spot in the top three), will share the $275 pool equally.

<br/>
<br/>
<br/>

## Preparing for the Quiz

<br/>

You have many resources at your disposal to prepare for the quiz.

All questions for these quizzes will come from the following resources, so reviewing them before a
quiz is highly recommended. The following links maybe useful:

1. [Transcripts of Yasir Qadhi's Videos](https://arqadhi.blogspot.com)
1. Ar-Raheeq Al-Makhtum (The Sealed Nectar) [English](https://drive.google.com/open?id=1SI5CJjUzvDjmJkdPH-_cIjD4n4mw9heG) or [Urdu](https://drive.google.com/open?id=1jLrcclFCOG8Y10tli71rfAUQFmpU-LZs)
1. [Videos from our Seerah Summary and Sahaba Series sessions](https://vimeo.com/showcase/7090720)
1. [Previous Presentations](https://www.dropbox.com/sh/o1pfuphqzn0hdd1/AAAvQIV1YXyrAc561FtRHizha?dl=0)
1. [The Biography of Abu Bakr Siddiq RA by As_-Sallabee](https://www.dropbox.com/s/tbnphqjteg52q1r/TheBiographyOfAbuBakrAs-siddeeq.pdf?dl=0)
1. [The Biography of Umar Ibn al-Khattab RA Part 1 by As_-Sallabee](https://www.dropbox.com/sh/o1pfuphqzn0hdd1/AAD9f2o9FR2RN5Bhu-MZpKAfa/Umar-Ibn-Al-khattab-Volume-1.pdf?dl=0)
1. [The Biography of Umar Ibn al-Khattab RA Part 2 by As_-Sallabee](https://www.dropbox.com/sh/o1pfuphqzn0hdd1/AADZLQx1sDnw_XryAIka6uqTa/Umar-Ibn-Al-khattab-Volume-2.pdf?dl=0)
1. [The Biography of Uthman Ibn Affan RA by As_-Sallabee](https://www.dropbox.com/s/bliymfpsh4qgpvk/TheBiographyOfUthmanIbnAffanrDhun-noorayn.pdf?dl=0)
1. [The Biography of Ali Ibn Abi Talib RA by Part 1 As_-Sallabee](https://www.dropbox.com/s/yfy79752opho4e6/ali-ibn-abi-talib-r-volume-1.pdf?dl=0)
1. [The Biography of Ali Ibn Abi Talib RA Part 2 by As_-Sallabee](https://www.dropbox.com/s/wztu2bk06u84dyu/ali-ibn-abi-talib-r-volume-2.pdf?dl=0)
1. [The Prophet PBUH and His Ten Companions Who Were Promised Paradise by Al Maqdisi](https://www.dropbox.com/s/n06ur6qojixw3s1/The-Prophet-His-Ten-Companions-Who-Were-Promised-Paradise-Abdul-Ghani-al-Maqdisi-High-Quality.pdf?dl=0)

<br/>

### Practice On Gimkit

The following quiz quizzes have been opened on Gimkit for you to practice offline:

[Quiz 1](https://www.gimkit.com/live/6056b928a68e650022689e2d)
[Quiz 2](https://www.gimkit.com/live/6056b948a68e650022689fed)
[Quiz 3](https://www.gimkit.com/live/6056b95da68e65002268a0b5)
[Quiz 4](	https://www.gimkit.com/live/6056b977a68e65002268a16b)
[Quiz 5](https://www.gimkit.com/live/6056b98da68e65002268a2fc)
[Quiz 6](https://www.gimkit.com/live/6056b9a6a68e65002268a420)

### Memorize on Cram.com

You can also use flashcards on Cram.com to review material here:
[Flashcards](http://www.cram.com/flashcards/ashratu-mubashira-12045985)

### Collaborate on Whatsapp

You can participate in a whatsapp group with the family: [here](https://chat.whatsapp.com/K09I51VYh3tEYyWHYyRMz4)
<br/>
<br/>


## Sahaba Grand Quiz

The Sahaba Grand Quiz will be a **12 minute quiz** held on the 20th March, 2021 based on the teams
model.

New teams (based on performance in the last five quizzes) will be announced for this quiz.

A new prize model will also be announced shortly for this Grand Quiz.

## Seerah Grand Quiz

<br/>

The Grand Quiz will be a **12 minute quiz** open to everyone (like all other quizzes).

The formula for calculating the Grand Prize Weighted Score will be as follows:

1. 50% of the weight will be the average of the participants score from the two preceding quizzes on 19th
   September, 2020 and 26th September, 2020

1. The other 50% of the weight will be to the actual score the participant achieves in the Grand
   Quiz itself

For example, if someone has the following scores:

1. Week 1 (19th September, 2020): **120**

2. Week 2 (26th September, 2020): **140**

3. Week 3 (3rd October, 2020): **200**

Then their weighted average will be: **(120+140)/2 + 200/2 = 230**

Participants will be ranked on their weighted average.


The prizes for the grand quiz will be as follows:

| Rank | Prize in USD | Prize in INR | Prize in SAR |
|------|--------------|--------------|--------------|
| 1st  | $300         | INR 22,000   | SAR 1100     |
| 2nd  | $250         | INR 18,000   | SAR 950      |
| 3rd  | $225         | INR 16,000   | SAR 850      |
| 4th  | $175         | INR 13,000   | SAR 650      |
| 5th  | $150         | INR 11,000   | SAR 550      |
| 6th  | $100         | INR 7,500   | SAR 400       |
| 7th  | $100         | INR 7,500   | SAR 400       |
| 8th  | $100         | INR 7,500   | SAR 400       |
| 9th  | $100         | INR 7,500   | SAR 400       |
| 10th  | $100         | INR 7,500   | SAR 400      |
| Participant | $25         | INR 2,000   | SAR 100 |

<br/>



## Grand Quiz Results


Grand Quiz Results from the Seerah Session are published [here](https://docs.google.com/spreadsheets/d/e/2PACX-1vTjSKMVBjHt5tcLKdGSMXqHhHekA14QuW4wdZQxyUIIzEu8sNOIC5MJ_fdUtEFhsTNoIwkp6Z1aDzZw/pubhtml?gid=0&single=true)
